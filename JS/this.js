/* Создайте объект calculator (калькулятор) с тремя методами:
read() (читать) запрашивает два значения и сохраняет их как свойства объекта.
sum() (суммировать) возвращает сумму сохранённых значений.
mul() (умножить) перемножает сохранённые значения и возвращает результат. */

let calculator = {
    read() {
        this.a = +prompt('Введите число a', '');
        this.b = +prompt('Введите число b', '');
    },
    sum() {
        return this.a + this.b;
    },
    mul() {
        return this.a * this.b;
    }
};
calculator.read()

console.log(`a + b равнo: ${calculator.sum()}`);
console.log(`a * b равнo: ${calculator.mul()}`);

/* Задача 2.
Это ladder (лестница) – объект, который позволяет подниматься вверх и спускаться:
let ladder = {
    step: 0,
    up() {
      this.step++;
    },
    down() {
      this.step--;
    },
    showStep: function() { // показывает текущую ступеньку
      alert( this.step );
    }
  };

  Измените код методов up, down и showStep таким образом, чтобы их вызов можно было
  сделать по цепочке, например так:
  ladder.up().up().down().showStep(); */

  let ladder = {
    step: 0,
    up() {
      this.step++;
      return this;
    },
    down() {
      this.step--;
      return this;
    },
    showStep() {
      console.log( this.step );
      return this;
    }
}