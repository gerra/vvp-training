/* Задача 1.
Что выведет следующий код? */

let fruits = ["Яблоки", "Груша", "Апельсин"];
let shoppingCart = fruits; // добавляем новое значение в "копию"
shoppingCart.push("Банан"); // через копию добавляем четвертый элемент к одному и тому же объекту
console.log(fruits.length); // 4

/* Задача 2.
Давайте произведём 5 операций с массивом.*/

//Создайте массив styles с элементами «Джаз» и «Блюз».
let styles = ['Джаз', 'Блюз'];
//Добавьте «Рок-н-ролл» в конец.
styles.push('Рок-н-ролл');
//Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
styles[Math.round((styles.length - 1) / 2)] = 'Classic';
// Удалите первый элемент массива и покажите его.
console.log(styles.shift());
// Вставьте «Рэп» и «Регги» в начало массива.
styles.unshift('Рэп', 'Рэгги');

console.log(styles);

/* Задача 3.
Каков результат? Почему? */

let arr = ["a", "b"];
arr.push(function() {
    console.log(this);
})
arr[2]();
//Так как arr является объектом, то мы обращаемся к его методу, this выводит сам объект, в итоге в консоль
//мы получаем (3) ['a', 'b', ƒ]

/* Задача 4.
Напишите функцию sumInput(), которая:
Просит пользователя ввести значения, используя prompt и сохраняет их в массив.
Заканчивает запрашивать значения, когда пользователь введёт не числовое значение, пустую строку или нажмёт «Отмена».
Подсчитывает и возвращает сумму элементов массива.
P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0». */

function sumInput() {
    let arrNum = [];
    let num = 0;
    while (true) {
        num = prompt('Введите число', 0);
        arrNum.push(+num);
        if (!num || !isFinite(num)) break;
    }
    let sum = 0;
    for (let number of arrNum) {
        sum += number;
    }
    return sum;
}

console.log(sumInput());

/* Задача 5.
На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].
Задача: найти непрерывный подмассив в arr, сумма элементов в котором максимальна.
Функция getMaxSubSum(arr) должна возвращать эту сумму.
Например:
getMaxSubSum([-1, 2, 3, -9]) = 5 (сумма выделенных)
getMaxSubSum([2, -1, 2, 3, -9]) = 6
getMaxSubSum([-1, 2, 3, -9, 11]) = 11
getMaxSubSum([-2, -1, 1, 2]) = 3
getMaxSubSum([100, -9, 2, -3, 5]) = 100
getMaxSubSum([1, 2, 3]) = 6 (берём все)
Если все элементы отрицательные – ничего не берём(подмассив пустой) и сумма равна «0»:
getMaxSubSum([-1, -2, -3]) = 0
Попробуйте придумать быстрое решение: O(n2), а лучше за О(n) операций. */

function getMaxSubSum(arr) {
    let maxSum = 0;
    let sum = 0;
    for (let number of arr) {
        sum += number;
        maxSum = Math.max(maxSum, sum);
        if (sum < 0) sum = 0;
    }
    return maxSum;
}

console.log(getMaxSubSum([-1, 2, 3, -9]));
console.log(getMaxSubSum([-1, 2, 3, -9, 11]));
console.log(getMaxSubSum([-2, -1, 1, 2]));
console.log(getMaxSubSum([100, -9, 2, -3, 5]));
console.log(getMaxSubSum([1, 2, 3]));
console.log(getMaxSubSum([-1, -2, -3]));