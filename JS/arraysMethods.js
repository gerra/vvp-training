/* Задача 1.
Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».
То есть дефисы удаляются, а все слова после них получают заглавную букву. */

let str;
function camelize(str) {
    let arr = str.split('-');
    let toUpperCaseArr = arr.map(
        (word, index) => index === 0
            ? word
            : `${word[0].toUpperCase()}${word.slice(1)}`
    );
    return str = toUpperCaseArr.join('');
}

console.log(camelize('my-short-string'));
console.log(camelize('list-style-image'));

/* Задача 2.
Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между
a и b и отдаёт массив этих элементов.
Функция должна возвращать новый массив и не изменять исходный. */

function filterRange(arr, a, b) {
    return arr.filter(item => (a <= item && item <= b));
}
let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);

console.log(arr);
console.log(filtered);

/* Задача 3.
Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr и удаляет из
него все значения кроме тех, которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.
Функция должна изменять принимаемый массив и ничего не возвращать. */

function filterRangeInPlace(arr, a, b) {
    arr.forEach((element, index) => {
        if (element >= a || element <= b) {
            arr.splice(index, 1);
        }
    });
}
let arr = [5, 3, 8, 1];
filterRangeInPlace(arr, 1, 4);

console.log(arr);

/* Задача 4.
Сортировать в пордке по убыванию */

let arr = [5, 2, 1, -10, 8];
arr.sort((a, b) => b - a);

console.log(arr);

/* Задача 5.
У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.
Создайте функцию copySorted(arr), которая будет возвращать такую копию. */

let copySorted = arr => arr.concat().sort();
let arr = ['HTML', 'JavaScript', 'CSS'];
let sorted = copySorted(arr);

console.log(arr);
console.log(sorted);

/* Задача 6.
Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.
Задание состоит из двух частей.
Во-первых, реализуйте метод calculate(str), который принимает строку типа '1 + 2' в формате
«ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.
Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции.
Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его. */

function Calculator() {
    this['+'] = (a, b) => +a + +b;
    this['-'] = (a, b) => a - b;
    this.calculate = (str) => {
        let arr = str.split(' ');
        return this[arr[1]](arr[0], arr[2]);
    }
    this.addMethod = (name, func) => this[name] = func;
}
let calc = new Calculator;
let powerCalc = new Calculator;
powerCalc.addMethod('*', (a, b) => a * b);
powerCalc.addMethod('/', (a, b) => a / b);
powerCalc.addMethod('**', (a, b) => a ** b);

console.log(calc.calculate('700 + 300'));
console.log(powerCalc.calculate('2 ** 3'));
console.log(powerCalc.calculate('6 / 3'));

/* Задача 7.
У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который
преобразует их в массив имён. */

let vasya = { name: 'Вася', age: 25 };
let petya = { name: 'Петя', age: 30 };
let masha = { name: 'Маша', age: 28 };
let users = [ vasya, petya, masha ];
let names = users.map(person => person.name);

console.log(names);

/* Задача 8.
У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.
Напишите код, который создаст ещё один массив объектов с параметрами id и fullName,
где fullName – состоит из name и surname. */

let vasya = { name: 'Вася', surname: 'Пупкин', id: 1 };
let petya = { name: 'Петя', surname: 'Иванов', id: 2 };
let masha = { name: 'Маша', surname: 'Петрова', id: 3 };
let users = [ vasya, petya, masha ];
let usersMapped = users.map(person => ({
        fullName: `${person.name} ${person.surname}`,
        id: person.id
    })
);

console.log(usersMapped[2].id);
console.log(usersMapped[2].fullName);

/* Задача 9.
Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему. */

let vasya = { name: 'Вася', age: 25 };
let petya = { name: 'Петя', age: 30 };
let masha = { name: 'Маша', age: 28 };
let arr = [ vasya, petya, masha ];
function sortByAge(users) {
    arr.sort((a, b) => a.age - b.age);
}
sortByAge(arr);

console.log(arr[0].name);
console.log(arr[1].name);
console.log(arr[2].name);
console.log(arr);

/* Задача 10.
Напишите функцию shuffle(array), которая перемешивает (переупорядочивает случайным образом) элементы массива.
Многократные прогоны через shuffle могут привести к разным последовательностям элементов.
Все последовательности элементов должны иметь одинаковую вероятность. Например, [1,2,3] может быть переупорядочено
как [1,2,3] или [1,3,2], или [3,1,2] и т.д., с равной вероятностью каждого случая. */

let arr = [1, 2, 3];
function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
//код для проверки..поэтому без отступов console.log
/* console.log(arr);
shuffle(arr);
console.log(arr);
shuffle(arr);
console.log(arr);
shuffle(arr);
console.log(arr); */

/* Задача 11.
Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством
age и возвращает средний возраст.
Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N. */

let vasya = { name: 'Вася', age: 25 };
let petya = { name: 'Петя', age: 30 };
let masha = { name: 'Маша', age: 29 };
let arr = [ vasya, petya, masha ];

function getAverageAge(users) {
    return users.reduce(
        (prev, person) => prev + person.age,
        0
    ) / users.length;
}

console.log(getAverageAge(arr));

/* Задача 12.
Пусть arr – массив строк.
Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr. */

function unique(arr) {
    let arrUnique = [];
    for (let item of arr) {
        if (!arrUnique.includes(item)) {
            arrUnique.push(item);
        }
    }
    return arrUnique;
}
let strings = ['кришна', 'кришна', 'харе', 'харе',
  'харе', 'харе', 'кришна', 'кришна', ':-O'
];

console.log(unique(strings));