/* Задача 1.
В коде ниже мы создаём нового кролика new Rabbit, а потом пытаемся изменить его прототип.
Сначала у нас есть такой код: */

function Rabbit() {}
Rabbit.prototype = {
    eats: true
};
let rabbit = new Rabbit();

console.log(rabbit.eats); // true

Rabbit.prototype = {};

console.log(rabbit.eats); // true, новый прототип не влияет на уже созданные объекты

// А если код такой (заменили одну строчку)?
function Rabbit() {}
Rabbit.prototype = {
    eats: true
};
let rabbit = new Rabbit();
Rabbit.prototype.eats = false; // изменили эту строчку

console.log(rabbit.eats); // false, потому что мы явно изменили метод выше

// Или такой (заменили одну строчку)?
function Rabbit() {}
Rabbit.prototype = {
    eats: true
};
let rabbit = new Rabbit();
delete rabbit.eats; // изменили эту строчку

console.log(rabbit.eats); // true, так как delete пытается удалить свойство объекта rabbit, которого там нет

// Или, наконец, такой:
function Rabbit() {}
Rabbit.prototype = {
    eats: true
};
let rabbit = new Rabbit();
delete Rabbit.prototype.eats;

console.log(rabbit.eats); // undefined, так как теперь Rabbit.prototype не существует

/* Задача 2.
Представьте, что у нас имеется некий объект obj, созданный функцией-конструктором –
мы не знаем какой именно, но хотелось бы создать ещё один объект такого же типа.
Можем ли мы сделать так?

let objTwo = new obj.constructor();

Приведите пример функции-конструктора для объекта obj, с которой такой вызов корректно
сработает. И пример функции-конструктора, с которой такой код поведёт себя неправильно. */

// так можно сделать если в фунции-конструкторе не удален сам метод конструктор
// вот пример из урока, где конструктор удален, из-за того что поменяли прототип

function Rabbit() {}
Rabbit.prototype = {
  jumps: true
};

let rabbit = new Rabbit();
console.log(rabbit.constructor === Rabbit); // false

// но если добавить конструктор, всё будет работать
function Rabbit() {}
Rabbit.prototype = {
  jumps: true,
  constructor: Rabbit
};

let rabbit = new Rabbit();
console.log(rabbit.constructor === Rabbit); // true