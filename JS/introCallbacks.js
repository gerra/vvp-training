/**
 * показывает анимацию круга
 * @param {*} cx
 * @param {*} cy
 * @param {*} radius
 * @param {*} callback колбэк-функция
 */
function showCircle(cx, cy, radius, callback) {
    let div = document.createElement('div');
    div.style.width = 0;
    div.style.height = 0;
    div.style.left = cx + 'px';
    div.style.top = cy + 'px';
    div.className = 'circle';
    document.body.append(div);

    setTimeout(
        () => {
            div.style.width = radius * 2 + 'px';
            div.style.height = radius * 2 + 'px';
            // добавим обработчик события
            div.addEventListener('transitionend', function handler() {
                div.removeEventListener('transitionend', handler);
                callback(div);
            });
        },
        0
    );
}

function start() {
    showCircle(
        150,
        150,
        100,
        div => {
            div.classList.add('circle__message');
            div.append('Hello, world!');
        }
    );
}