/* Задача 1.
Создайте декоратор spy(func), который должен возвращать обёртку, которая сохраняет
все вызовы функции в своём свойстве calls.
Каждый вызов должен сохраняться как массив аргументов. */
console.log('Задача 1.');

function work(a, b) {
    console.log(a + b);
}

work = spy(work);
work(1, 2);
work(4, 5);

for (let args of work.calls) {
    console.log(`calls: ${args.join(', ')}`);
}

function spy(func) {
    function wrapper(...args) {
        wrapper.calls.push(args);
        return func.apply(this, arguments);
    }
    wrapper.calls = [];
    return wrapper;
}

/* Задача 2.
Создайте декоратор delay(f, ms), который задерживает каждый вызов f на ms миллисекунд.
Другими словами, delay(f, ms) возвращает вариант f с «задержкой на ms мс».
В приведённом выше коде f – функция с одним аргументом, но ваше решение должно передавать
все аргументы и контекст this. */
console.log('Задача 2.');

/* function delay(f, ms) {
    return function(...args) { // функция может принять в себя любое количество параметров
        let savedThis = this; // сохраняем this в промежуточную переменную
        setTimeout(
            function() {
                f.apply(savedThis, args); // используем её и возвращаем результат с заданым this
            },
            ms
        );
    };
} */
// Второй вариант. Через стрелочную функцию.
function delay(f, ms) {
    return function() {
        setTimeout(() => f.apply(this, arguments), ms); // берет this и аргументы из обертки, т.к. не имеет своих
    };
}

function f(x) {
    console.log(x);
}  
// создаём обёртки
let f1000 = delay(f, 1000);
let f1500 = delay(f, 1500);
f1000("test"); // показывает "test" после 1000 мс
f1500("test"); // показывает "test" после 1500 мс

/* Задача 3.
Результатом декоратора debounce(f, ms) должна быть обёртка, которая передаёт
вызов f не более одного раза в ms миллисекунд. Другими словами, когда мы вызываем
debounce, это гарантирует, что все остальные вызовы будут игнорироваться в течение ms. */

console.log('Задача 3.');

function debounce(f, ms) {
    let isCooldown = false; // в первом вызове false и функция продолжит выполнение
    return function() {
        if (isCooldown) return; // вызов проходит потому что false, далее ожидание окончания тайм-аута
        f.apply(this, arguments); // выполение фунции 
        isCooldown = true; // присваиваем true для ожидания окончания тайм-аута
        setTimeout(() => isCooldown = false, ms); // после заданной задержки меняем на false
    };
}

function f(phrase) { 
    console.log(`${phrase}`);
};

f = debounce(f, 1000);

setTimeout(
    () => f('тест 1.'), // Выполняется.
    2000
);

setTimeout(
    () => f('тест 2.'), // Игнорируется.
    2500
);

setTimeout(
    () => f('тест 3.'), // Выполняется.
    3500
);

/* Задача 4.
Создайте «тормозящий» декоратор throttle(f, ms), который возвращает обёртку, передавая
вызов в f не более одного раза в ms миллисекунд. Те вызовы, которые попадают в период
«торможения», игнорируются. */

console.log('Задача 4.');

function funcConsole(a) {
    console.log(a);
}

/* function throttle(funcConsole, gap) {
    let lastTime = 0;
    let timer;
    return function(...args) {
        clearTimeout(timer);
        if (Date.now() - lastTime > gap) {
            funcConsole.apply(this, args);
            lastTime = Date.now();
        } else {
            timer = setTimeout(
                () => {
                    funcConsole.apply(this, args);
                    lastTime = Date.now();
                },
                gap - (Date.now() - lastTime)
            );
        }
    }
} */
// через тернарку
function throttle(funcConsole, gap) {
    let lastTime = 0;
    let timer;
    return function(...args) {
        clearTimeout(timer);
        (Date.now() - lastTime > gap) 
            ? (funcConsole.apply(this, args), lastTime = Date.now())
            : timer = setTimeout(
                () => {
                    funcConsole.apply(this, args);
                    lastTime = Date.now();
                },
                gap - (Date.now() - lastTime)                   
            );
    }
}

let console1000 = throttle(funcConsole, 1000);

setTimeout(
    () => console1000(1), // Показывает 1
    4000
);

setTimeout(
    () => console1000(2), // Не выводит
    4000
);

setTimeout(
    () => console1000(3), // Показывает 3 через 1 секунду
    4000
);