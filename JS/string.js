/* Задача 1.
Напишите функцию ucFirst(str), возвращающую строку str с заглавным первым символом. */

/* function ucFirst(str) {
    let newStr = `${str[0].toUpperCase()}${str.slice(1)}`;
    return newStr;
}

console.log(ucFirst('вася'));
console.log(ucFirst('петя'));
console.log(ucFirst('зелибоба')); */

/* Задача 2.
Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или 'XXX',
а иначе false. */

function checkSpam(str) {
    let newStr = str.toLowerCase();
    return newStr.includes('viagra') || newStr.includes('xxx'); //ищет подстроку в строке, возвращает true если находит
}

console.log(checkSpam('valeryanka smuglyanka hop hey la la ley')); //false
console.log(checkSpam('Bla bla lol kek viagra')); //true
console.log(checkSpam('Bla bla XXX lol kek')); //true
console.log(checkSpam('Bla bla ViAgRA lol kek')); //true

/* Задача 3.
Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, если она
превосходит maxlength, заменяет конец str на "…", так, чтобы её длина стала равна maxlength.
Результатом функции должна быть та же строка, если усечение не требуется, либо, если необходимо,
усечённая строка. */

/* function truncate(str, maxlength) {
    return (str.length < maxlength)
        ? str
        : `${str.slice(0, maxlength - 1)}…`;
}

console.log(truncate('если усечение не требуется, либо, если необходимо', 20));
console.log(truncate('если усечение не требуется, либо, если необходимо', 80)); */

/* Задача 4.
Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем – число.
Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять числовое значение
и возвращать его. */

/* function extractCurrencyValue(str) {
    return +str.slice(1);
}

console.log(extractCurrencyValue('$120'));
console.log(extractCurrencyValue('$12.5')); */