/* Задача 1.
Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

Например:
sumTo(1) = 1
sumTo(2) = 2 + 1 = 3
sumTo(3) = 3 + 2 + 1 = 6
sumTo(4) = 4 + 3 + 2 + 1 = 10
...
sumTo(100) = 100 + 99 + ... + 2 + 1 = 5050

Сделайте три варианта решения:
С использованием цикла.
Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
С использованием формулы арифметической прогрессии.
P.S. Какой вариант решения самый быстрый? Самый медленный? Почему?
P.P.S. Можно ли при помощи рекурсии посчитать sumTo(100000)? */

// Вариант 1.
function sumTo(n) {
    let result = 0;
    for (let i = 1; i <= n; i++) {
        result += i;
    }
    return result;
}

console.log(sumTo(1));
console.log(sumTo(2));
console.log(sumTo(4));
console.log(sumTo(100));

// Вариант 2.
function sumToRec(n) {
    return (n === 1)
        ? n
        : n + sumToRec(n - 1);
}

console.log(sumToRec(1));
console.log(sumToRec(2));
console.log(sumToRec(4));
console.log(sumToRec(100));
// console.log(sumToRec(100000)); ошибка, превышает максимальный размер стака

// Вариант 3.
function sumToArith(n) {
    return n * (n + 1) / 2;
}

console.log(sumToRec(1));
console.log(sumToRec(2));
console.log(sumToRec(4));
console.log(sumToRec(100));

/* Задача 2.
Факториал натурального числа – это число, умноженное на "себя минус один",
затем на "себя минус два", и так далее до 1. Факториал n обозначается как n!
Задача – написать функцию factorial(n), которая возвращает n!, используя рекурсию.
P.S. Подсказка: n! можно записать как n * (n-1)! Например: 3! = 3*2! = 3*2*1! = 6 */

function factorial(n) {
    return (n === 1)
        ? n
        : n * factorial(n-1);
}

console.log(factorial(1));
console.log(factorial(3));
console.log(factorial(5));

/* Задача 3.
Последовательность чисел Фибоначчи определяется формулой Fn = Fn-1 + Fn-2.
То есть, следующее число получается как сумма двух предыдущих.
Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 5(2+3) и так далее: 1, 1, 2, 3, 5, 8, 13, 21....
Числа Фибоначчи тесно связаны с золотым сечением и множеством природных явлений вокруг нас.
Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.
P.S. Все запуски функций из примера выше должны работать быстро. Вызов fib(77) должен занимать не более доли секунды. */

function fib(n) {
    let fibonacci = [1, 1];
    for (i = 2; i < n; i++) {
        fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
    }
    return (n < 2)
        ? 1
        : parseInt(fibonacci.slice(-1));
}

console.log(fib(1));
console.log(fib(7));
console.log(fib(77));

/* Задача 4.
Допустим, у нас есть односвязный список (как описано в главе Рекурсия и стек):
Напишите функцию printList(list), которая выводит элементы списка по одному.
Сделайте два варианта решения: используя цикл и через рекурсию.
Как лучше: с рекурсией или без? */

let list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};
// Вариант 1. Цикл
function printList(list) {
    while (list) {
        console.log(list.value);
        list = list.next;
    }
}
printList(list);

// Вариант 2. Рекурсия
function printListRec(list) {
    console.log(list.value);
    if (list.next) {
        printListRec(list.next);
    }
}

printListRec(list);

/* Задача 5.
Выведите односвязный список из предыдущего задания Вывод односвязного списка в обратном порядке.
Сделайте два решения: с использованием цикла и через рекурсию. */

// Цикл
function printReverseList(list) {
    let arr = [];
    while (list) {
        arr.push(list.value);
        list = list.next;
    }
    arr = arr.reverse();
    for (let item of arr) {
        console.log(item);
    } 
}
printReverseList(list);

// Рекурсия
function printReverseListRec(list) {
    if (list.next) {
        printReverseListRec(list.next);
    }
    console.log(list.value);
}
printReverseListRec(list);