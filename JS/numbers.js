/* Задача 1.
Создайте скрипт, который запрашивает ввод двух чисел (используйте prompt)
и после показывает их сумму. */

let a = +prompt('Введите a', '0');
let b = +prompt('Введите b', '0');
let sum = a + b;

console.log(sum);

/* Задача 2.
Методы Math.round и toFixed, согласно документации, округляют до ближайшего
целого числа: 0..4 округляется в меньшую сторону, тогда как 5..9 в большую сторону.

Например:
console.log(1.35.toFixed(1));

Но почему в примере ниже 6.35 округляется до 6.3?
console.log(6.35.toFixed(1));

Потому что toFixed(1) окруляет до 1 цифры после запятой, чтобы округлить до двух
цифр после запятой нужно указать toFixed(2)
console.log(6.35.toFixed(2)); */

/* Задача 3.
Создайте функцию readNumber, которая будет запрашивать ввод числового значения
до тех пор, пока посетитель его не введёт.
Функция должна возвращать числовое значение.
Также надо разрешить пользователю остановить процесс ввода, отправив пустую строку
или нажав «Отмена». В этом случае функция должна вернуть null. */

function readNumber() {
    let num;
    do {
        num = prompt('Введите число', 0);
    } while (!isFinite(num)); //проверяет содержится ли в строке число
    return (!num || num === ' ')
        ? null
        : +num;
}

console.log(readNumber());

/* Задача 4.
Этот цикл – бесконечный. Он никогда не завершится, почему?

let i = 0;
while (i != 10) {
  i += 0.2;
}
Я думаю из-за погрешности при потере точности. */

/* Задача 5.
Встроенный метод Math.random() возвращает случайное число
от 0 (включительно) до 1 (но не включая 1)
Напишите функцию random(min, max), которая генерирует случайное
число с плавающей точкой от min до max (но не включая max). */

function getRandom(a, b) {
    let min = a < b
        ? a
        : b;
    let max = a > b
        ? a
        : b;
    return Math.random() * (max - min) + min;
}

console.log(getRandom(1, 5));
console.log(getRandom(1, 5));
console.log(getRandom(1, 5));

/* Задача 6.
Напишите функцию randomInteger(min, max), которая генерирует случайное
целое (integer) число от min до max (включительно).
Любое число из интервала min..max должно появляться с одинаковой вероятностью. */

function randomInteger(a, b) {
    min = a < b
        ? Math.ceil(a)
        : Math.ceil(b); //тут в зависимости от потребности можно округлить и в меньшую сторону
    max = a > b
        ? Math.floor(a)
        : Math.floor(b);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log(randomInteger(5, 1));
console.log(randomInteger(5, 1));
console.log(randomInteger(5, 1));
console.log(randomInteger(1.5, 7));