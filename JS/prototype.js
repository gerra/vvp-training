/* Задача 1.
В приведённом ниже коде создаются и изменяются два объекта.
Какие значения показываются в процессе выполнения кода? */

let animal = {
    jumps: null
};
let rabbit = {
    __proto__: animal,
    jumps: true
};

console.log(rabbit.jumps); // ? (1) True

delete rabbit.jumps;

console.log(rabbit.jumps); // ? (2) null

delete animal.jumps;

console.log(rabbit.jumps); // ? (3) undefined

/* Задача 2.
У нас есть объекты:
С помощью свойства __proto__ задайте прототипы так,
чтобы поиск любого свойства выполнялся по следующему пути:
pockets → bed → table → head. Например, pockets.pen должно
возвращать значение 3 (найденное в table), а bed.glasses –
значение 1 (найденное в head).
Ответьте на вопрос: как быстрее получить значение glasses –
через pockets.glasses или через head.glasses? При необходимости
составьте цепочки поиска и сравните их. */

let head = {
    glasses: 1
};
let table = {
    pen: 3,
    __proto__: head
};
let bed = {
    sheet: 1,
    pillow: 2,
    __proto__: table
};
let pockets = {
    money: 2000,
    __proto__: bed
};

/* По скорости без разницы как получить значение glasses, потому что 
движок запоминает где нашел значение и в следующий раз берет его оттуда
сразу. */

/* Задача 3.
Объект rabbit наследует от объекта animal.
Какой объект получит свойство full при вызове rabbit.eat(): animal или rabbit? */

let animalOne = {
    eat() {
        this.full = true; // animalOne получит свойство full только после вызова animalOne.eat()
    }
};

let rabbitOne = {
    __proto__: animalOne
};

rabbitOne.eat(); // объект rabbitOne получает свойство full

console.log(rabbitOne.full); // true

/* Задача 4.
У нас есть два хомяка: шустрый (speedy) и ленивый (lazy);
оба наследуют от общего объекта hamster.
Когда мы кормим одного хомяка, второй тоже наедается. Почему?
Как это исправить? */

/* let hamster = {
    stomach: [],
  
    eat(food) {
        this.stomach.push(food);
    }
};
let speedy = {
    __proto__: hamster
};
let lazy = {
    __proto__: hamster
};

// Этот хомяк нашёл еду
speedy.eat("apple");

console.log(speedy.stomach); // apple

// У этого хомяка тоже есть еда. Почему? Исправьте
console.log(lazy.stomach); // apple */

// Решение
let hamster = {
    stomach: [], // у хомяков был один общий желудок, который они наследовали
  
    eat(food) {
        this.stomach.push(food);
    }
};
// сделаем каждому хомяку свой собственный желудок

let speedy = {
    stomach: [],
    __proto__: hamster
};
let lazy = {
    stomach: [],
    __proto__: hamster
};

// Этот хомяк нашёл еду
speedy.eat("apple");

console.log(speedy.stomach); // apple

console.log(lazy.stomach); // пусто