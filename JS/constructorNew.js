/* Задача 1.
Возможно ли создать функции A и B в примере ниже, где объекты равны new A()==new B()?

function A() { ... }
function B() { ... }
let a = new A;
let b = new B;

console.log(a == b); // true

// Да, возможно.
let obj = {};
function A() { return obj; }
function B() { return obj; }

console.log(new A() == new B()); // true */

/* Задача 2. 
Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
read() запрашивает два значения при помощи prompt и сохраняет их значение в
свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств. */

function Calculator() {
    this.read = function() {
        this.a = +prompt('Введите число a', '1');
        this.b = +prompt('Введите число b', '2');
    };
    this.sum = function() {
        return this.a + this.b;
    };
    this.mul = function() {
        return this.a * this.b;
    };
}
let calculator = new Calculator();
calculator.read();

console.log(`a + b равнo: ${calculator.sum()}`);
console.log(`a * b равнo: ${calculator.mul()}`);

/* Задача 3.
Напишите функцию-конструктор Accumulator(startingValue).
Объект, который она создаёт, должен уметь следующее:
Хранить «текущее значение» в свойстве value. Начальное значение устанавливается
в аргументе конструктора startingValue.
Метод read() использует prompt для получения числа и прибавляет его к свойству value.
Таким образом, свойство value является текущей суммой всего, что ввёл пользователь
при вызовах метода read(), с учётом начального значения startingValue. */

function Accumulator(startingValue) {
    this.value = startingValue;
    this.read = function() {
        this.value += +prompt('Введите число', '1');
    };
}
let accumulator = new Accumulator(1);
accumulator.read();
accumulator.read();

console.log(accumulator.value);