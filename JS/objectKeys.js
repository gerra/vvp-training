/* Задача 1.
Есть объект salaries с произвольным количеством свойств, содержащих заработные платы.
Напишите функцию sumSalaries(salaries), которая возвращает сумму всех зарплат
с помощью метода Object.values и цикла for..of.
Если объект salaries пуст, то результат должен быть 0. */

let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250
};

function sumSalaries(salaries) {
    let maxSum = 0;
    for (salary of Object.values(salaries)) {
        maxSum += salary;
    }
    return maxSum;
}

console.log(sumSalaries(salaries));

/* Задача 2.
Напишите функцию count(obj), которая возвращает количество свойств объекта.
Постарайтесь сделать код как можно короче.
P.S. Игнорируйте символьные свойства, подсчитывайте только «обычные». */

let user = {
    name: 'John',
    age: 30
};

function count(obj) {
    return Object.values(obj).length;
}

console.log(count(user));