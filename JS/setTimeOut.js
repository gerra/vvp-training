/* Задача 1.
Напишите функцию printNumbers(from, to), которая выводит число каждую секунду,
начиная от from и заканчивая to.
Сделайте два варианта решения.
Используя setInterval.
Используя рекурсивный setTimeout. */

function printNumbers(from, to) {
    let current = from;
    let timeId = setInterval(
        function () {
            console.log(current);
            if (current === to) {
                clearInterval(timeId);
            }
            current++;
        },
        1000
    );
}
printNumbers(1, 5);

function printNumbersRec (from, to) {
    let current = from;
    setTimeout(
        function start() {
            console.log(current);
            if (current < to) {
                setTimeout(start, 1000);
            }
            current++;
        },
        1000
    );
}
printNumbersRec (1, 5);