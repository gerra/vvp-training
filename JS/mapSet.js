/* Задача 1.
Допустим, у нас есть массив arr.
Создайте функцию unique(arr), которая вернёт массив уникальных,
не повторяющихся значений массива arr. */

function unique(arr) {
    return Array.from(new Set(arr));
}
let values = ['Hare', 'Krishna', 'Hare', 'Krishna',
    'Krishna', 'Krishna', 'Hare', 'Hare', ':-O'
];

console.log(unique(values));

/* Задача 2.
Отфильтруйте анаграммы.
Например:
nap - pan
ear - are - era
cheaters - hectares - teachers
Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм.
Из каждой группы анаграмм должно остаться только одно слово, не важно какое. */

let arr = ['nap', 'teachers', 'cheaters',
    'PAN', 'ear', 'era', 'hectares'
];

let map = new Map();
function aclean(arr) {
    let map = new Map();
    let sortItem;
    arr.forEach(item => {
        sortItem = item
            .toLowerCase()
            .split('')
            .sort()
            .join('');
        map.set(sortItem, item);
    });
    return Array.from(map.values());
}

console.log(arr);
console.log(aclean(arr));

/* Задача 3.
Мы хотели бы получить массив ключей map.keys() в переменную и далее
работать с ними, например, применить метод .push.
Но это не выходит: */

let map = new Map();
map.set('name', 'John');
let keys = map.keys();
//нужно создать массив через Array.from(map.keys()), тогда все будет работать
// Error: keys.push is not a function
// Ошибка: keys.push -- это не функция
keys.push('more');