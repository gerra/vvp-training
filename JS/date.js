/* Задача 1.
Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут.
Временная зона – местная. */

let newDate = new Date(2012, 1, 20, 3, 12);

console.log(newDate);

/* Задача 2.
Напишите функцию getWeekDay(date), показывающую день недели в коротком
формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», «СБ», «ВС». */

let date = new Date(2021, 08, 27);
function getWeekDay(date) {
    let dayOfWeek = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
    return dayOfWeek[date.getDay()];
}

console.log(getWeekDay(date));

/* Задача 3.
В Европейских странах неделя начинается с понедельника (день номер 1),
затем идёт вторник (номер 2) и так до воскресенья (номер 7).
Напишите функцию getLocalDay(date), которая возвращает «европейский»
день недели для даты date */

let date = new Date(2021, 8, 26); // это воскресенье
function getLocalDay(date) {
    let euDay = date.getDay();
    return euDay === 0
        ? euDay + 7
        : euDay;
}

console.log(getLocalDay(date)); // 7

/* Задача 4.
Создайте функцию getDateAgo(date, days), возвращающую число, которое
было days дней назад от даты date.
К примеру, если сегодня двадцатое число, то getDateAgo(new Date(), 1)
вернёт девятнадцатое и getDateAgo(new Date(), 2) – восемнадцатое.
Функция должна надёжно работать при значении days=365 и больших значениях.
P.S. Функция не должна изменять переданный ей объект date. */

let date = new Date(2021, 8, 27);
function getDateAgo(date, days) {
    let newDate = new Date(date);
    newDate.setDate(date.getDate() - days);
    return newDate.getDate();
}

console.log(getDateAgo(date, 2));
console.log(getDateAgo(date, 5));
console.log(getDateAgo(date, 380));

/* Задача 5.
Напишите функцию getLastDayOfMonth(year, month), возвращающую последнее
число месяца. Иногда это 30, 31 или даже февральские 28/29.
Параметры:
year – год из четырёх цифр, например, 2012.
month – месяц от 0 до 11.
К примеру, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль). */

function getLastDayOfMonth(year, month) {
    let date = new Date(year, month + 1, 0);
    return date.getDate();
}

console.log(getLastDayOfMonth(2021, 9)); // 31
console.log(getLastDayOfMonth(2021, 10)); // 30

/* Задача 6.
Напишите функцию getSecondsToday(), возвращающую количество секунд с начала
сегодняшнего дня.
Например, если сейчас 10:00, и не было перехода на зимнее/летнее время, то:
getSecondsToday() == 36000 // (3600 * 10)
Функция должна работать в любой день, т.е. в ней не должно быть конкретного
значения сегодняшней даты. */

function getSecondsToday() {
    let newDate = new Date();
    return newDate.getHours() * 3600 + newDate.getMinutes() * 60 + newDate.getSeconds();
}

console.log(getSecondsToday());

/* Задача 7.
Создайте функцию getSecondsToTomorrow(), возвращающую количество секунд до
завтрашней даты.
Например, если сейчас 23:00, то:
getSecondsToTomorrow() == 3600
P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного
значения сегодняшней даты. */

function getSecondsToTomorrow() {
    let now = new Date();
    let tomorrow = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() + 1
    );
    let diff = tomorrow - now;
    return Math.round(diff / 1000); // переводим мс в сек
}

console.log(getSecondsToTomorrow());

/* Задача 8.
Напишите функцию formatDate(date), форматирующую date по следующему принципу:
Если спустя date прошло менее 1 секунды, вывести "прямо сейчас".
В противном случае, если с date прошло меньше 1 минуты, вывести "n сек. назад".
В противном случае, если меньше часа, вывести "m мин. назад".
В противном случае, полная дата в формате "DD.MM.YY HH:mm". А именно:
"день.месяц.год часы:минуты", всё в виде двух цифр, т.е. 31.12.16 10:00. */

function formatDate(date) {
    let diff = new Date() - date;
    let sec = Math.round(diff / 1000) // сек
    let min = Math.round(diff / 60000); // минута
    let d = date;
    d = [
        `0${d.getDate()}`,
        `0${(d.getMonth() + 1)}`,
        `${d.getFullYear()}`,
        `0${d.getHours()}`,
        `0${d.getMinutes()}`
    ].map(item => item.slice(-2));
    if (diff < 1000) {
        return 'прямо сейчас';
    }
    if (sec < 60) {
        return `${sec} сек. назад`;
    }
    if (min < 60) {
        return `${min} мин. назад`;
    }
    return `${d.slice(0, 3).join('.')} ${d.slice(3).join(':')}`;
}

console.log(formatDate(new Date(new Date - 1)));
console.log(formatDate(new Date(new Date - 30 * 1000)));
console.log(formatDate(new Date(new Date - 5 * 60 * 1000)));
console.log(formatDate(new Date(new Date - 86400 * 1000)));